#include "jackproxythread.h"

JackProxyThread * JackProxyThread::m_singleton = new JackProxyThread();

JackProxyThread::JackProxyThread(QObject *parent) :
    QThread(parent)
{
}

JackProxyThread* JackProxyThread::singleton()
{
    return m_singleton;
}
