#ifndef JACKPROXYTHREAD_H
#define JACKPROXYTHREAD_H

#include <QThread>
#include "jackcommon.h"

class JackProxyThread : public QThread
{
    Q_OBJECT
    static JackProxyThread *m_singleton;
public:
    explicit JackProxyThread(QObject *parent = 0);
    int callback(jack_transport_state_t state,
                 jack_nframes_t nframes,
                 jack_position_t *pos,
                 int new_pos,
                 void *arg)
    {
        emit callbackSignal( state,nframes,pos,new_pos,arg);
    }
    static JackProxyThread* singleton();
signals:
    void callbackSignal(jack_transport_state_t state,
                        jack_nframes_t nframes,
                        jack_position_t *pos,
                        int new_pos,
                        void *arg);
public slots:
    
};

#endif // JACKPROXYTHREAD_H
