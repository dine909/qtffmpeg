#ifndef FRAMEIMAGEBUFFER_H
#define FRAMEIMAGEBUFFER_H

#include <QObject>
#include <QDebug>
#include <QImage>
#include <QSemaphore>
#include <QQueue>
#include <frameimage.h>

class FrameImageBuffer : public QObject
{
    Q_OBJECT
    QSemaphore sem;
     QSemaphore dsem;
    QQueue<FrameImage>images;
     int ctr;
public:
    explicit FrameImageBuffer(QObject *parent = 0);

signals:
    void videoOutput(QImage);
public slots:
    void frameClock();
    void frameClock(ulong frame);
    virtual void writeSlot(FrameImage data);

};

#endif // FRAMEIMAGEBUFFER_H
