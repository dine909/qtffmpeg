#ifndef AVGRAPHICSWIDGET_H
#define AVGRAPHICSWIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QResizeEvent>

class AVGraphicsWidget : public QGraphicsView
{
    Q_OBJECT
public:
    explicit AVGraphicsWidget(QWidget *parent = 0);
    QGraphicsScene scene;
    void resizeEvent(QResizeEvent *event);
signals:
        void resizedSignal(QSize s);
public slots:
//    void
};

#endif // AVGRAPHICSWIDGET_H
