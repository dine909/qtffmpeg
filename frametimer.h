#ifndef FRAMETIMER_H
#define FRAMETIMER_H

#include <QThread>

class FrameTimer : public QObject
{
    Q_OBJECT
    int m_usDelay;

public:
    Q_PROPERTY(int usDelay READ usDelay WRITE setUsDelay)
explicit FrameTimer(QObject *parent = 0);
int usDelay() const
{
    return m_usDelay;
}

public slots:
    void process();
signals:
    void timeout();
public slots:
    
    void setUsDelay(int arg)
    {
        m_usDelay = arg;
    }
};

#endif // FRAMETIMER_H
