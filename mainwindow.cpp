#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) , demuxer(0), buffer(0)
{

    avcodec_register_all();

    avg=new AVGraphicsWidget(0);
    this->setCentralWidget(avg);
    videoObject=new AVVideoObject(0);
    avg->scene.addItem(videoObject);

    QThread *st=new QThread(0);
    showtime=new JackShowTime(0);
    showtime->moveToThread(st);
    connect(st,SIGNAL(started()),showtime,SLOT(process()));

    QThread *dt=new QThread(0);
    demuxer=new Demuxer(0);
    demuxer->moveToThread(dt);
    connect(dt,SIGNAL(started()),demuxer,SLOT(process()));

//    QThread *ct=new QThread(0);
    buffer=new FrameImageBuffer(0);
//    buffer->moveToThread(ct);
//    connect(ct,SIGNAL(started()),buffer,SLOT(process()));

    QThread *ft=new QThread(0);
    ftimer=new FrameTimer(0);
    ftimer->moveToThread(ft);
    ftimer->setUsDelay(33333);
    connect(ft,SIGNAL(started()),ftimer,SLOT(process()));


    connect(demuxer,SIGNAL(writeFrameImage(FrameImage)),
            buffer,SLOT(writeSlot(FrameImage)),Qt::DirectConnection);

//    connect(buffer,SIGNAL(setRequestFrames(int)),
//            demuxer,SLOT(setRequestFrames(int)),Qt::DirectConnection);

    connect(ftimer,SIGNAL(timeout()),
            buffer,SLOT(frameClock()),Qt::DirectConnection);

    connect(showtime,SIGNAL(frameClock(ulong)),
            buffer,SLOT(frameClock(ulong)),Qt::DirectConnection);

    connect(buffer,SIGNAL(videoOutput(QImage)),
            videoObject,SLOT(videoInput(QImage)),Qt::DirectConnection);

    //    consumer->setFilename("/Users/dine/Documents/SWD5/QtFFMPEGExample/tests/lick.vid");
    //    decoder.videoDecodeExample("/Users/dine/Downloads/testout.mpg", "/Users/dine/Downloads/ROUND1.MPG");
    char* ins[]={"/Users/dine/Downloads/DOHA_01_2560_h264.mov",
                 "/Users/dine/Documents/SWD5/QtFFMPEGExample/lick.aud"};
    demuxer->init(ins);

  dt->start();
 //   ft->start();
    st->start();
}

MainWindow::~MainWindow()
{

}
