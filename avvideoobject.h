#ifndef AVVIDEOOBJECT_H
#define AVVIDEOOBJECT_H

#include <ffmpegcommon.h>
#include <QObject>
#include <QGraphicsObject>
#include <QMutex>

class AVVideoObject : public QGraphicsObject
{
    Q_OBJECT
    QMutex mutex;


public:
    explicit AVVideoObject(QGraphicsItem *parent = 0);
    QImage image;
    int iwidth,iheight;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
signals:
public slots:

//    void videoInput(AVVideoFrame frame);
      void videoInput(QImage frame);
public slots:
    
};

#endif // AVVIDEOOBJECT_H
