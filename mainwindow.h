#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ffmpegcommon.h>
#include <QThread>
#include <QApplication>
#include <avgraphicswidget.h>
#include <avvideoobject.h>
#include <frametimer.h>
#include <frameimagebuffer.h>
#include <imagebuffer.h>
#include <jackshowtime.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Demuxer *demuxer;
    FrameImageBuffer *buffer;
    AVGraphicsWidget *avg;
    AVVideoObject *videoObject;
    JackShowTime *showtime;
    FrameTimer *ftimer;

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H
