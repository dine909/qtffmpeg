#include "demuxer.h"

Demuxer::Demuxer(QObject *parent) :
    QObject(parent),
    sws_ctx(NULL)
  //    m_runstate(0)
{
    fmt_ctx = NULL;
    video_dec_ctx = NULL;
    audio_dec_ctx=NULL;
    video_stream = NULL;
    audio_stream = NULL;
    src_filename = NULL;
    audio_dst_filename = NULL;
    audio_dst_file = NULL;

    //    video_dst_data = {NULL};
    memset(video_dst_data,NULL,sizeof(video_dst_data));
    video_dst_linesize[4];
    video_dst_bufsize;

    audio_dst_data = NULL;
    audio_dst_linesize;
    audio_dst_bufsize;

    video_stream_idx = -1, audio_stream_idx = -1;
    frame = NULL;
    pkt;
    video_frame_count = 0;
    audio_frame_count = 0;

}

int Demuxer::decodePacket(int *got_frame, int cached)
{
    int ret = 0;
    if (pkt.stream_index == video_stream_idx) {
        /* decode video frame */
        if(pkt.dts != AV_NOPTS_VALUE) {
            pts = pkt.dts;
        } else {
            pts = 0;
        }
        pts *= av_q2d(video_stream->time_base);


        ret = avcodec_decode_video2(video_dec_ctx, frame, got_frame, &pkt);
        if (ret < 0) {
            fprintf(stderr, "Error decoding video frame\n");
            return ret;
        }

        if (*got_frame) {

            printf("video_frame%s n:%d coded_n:%d pts:%s (cpts:%d)\n",
                   cached ? "(cached)" : "",
                   video_frame_count++, frame->coded_picture_number,
                   av_ts2timestr(frame->pts, &video_dec_ctx->time_base),pts);

            /* copy decoded frame to destination buffer:
             * this is required since rawvideo expects non aligned data */

            /* write to rawvideo file */
            //            fwrite(video_dst_data[0], 1, video_dst_bufsize, video_dst_file);

            //            av_freep(&dst_data[0]);
            //            sws_freeContext(sws_ctx);
            //            if(isSignalConnected(QMetaMethod::fromSignal(&Demuxer::writeRgb24))){
            //                AVVideoFrame vframe;

            //                char bo[out_size];
            //                memcpy(bo,dst_data[0], out_size);

            //                QByteArray baout((const char*)bo, out_size);
            //                vframe.data=baout;
            //                vframe.height=video_dec_ctx->height;
            //                vframe.width=video_dec_ctx->width;
            //                vframe.format=video_dec_ctx->pix_fmt;
            //                emit writeRgb24(vframe);
            //            }
            av_image_copy(video_dst_data, video_dst_linesize,
                          (const uint8_t **)(frame->data), frame->linesize,
                          video_dec_ctx->pix_fmt, video_dec_ctx->width, video_dec_ctx->height);

            if(!sws_ctx){
                out_size = av_image_alloc(dst_data, dst_linesize,
                                          video_dec_ctx->width,  video_dec_ctx->height, PIX_FMT_RGB32, 1);
                Q_ASSERT(out_size>0);

                sws_ctx = sws_getContext(video_dec_ctx->width, video_dec_ctx->height, video_dec_ctx->pix_fmt,
                                         video_dec_ctx->width, video_dec_ctx->height, PIX_FMT_RGB32,
                                         SWS_BILINEAR, NULL, NULL, NULL);

            }

            sws_scale(sws_ctx, (const uint8_t **)(video_dst_data),
                      video_dst_linesize, 0,  video_dec_ctx->height, dst_data, dst_linesize);

            QImage im=QImage((uchar*)dst_data[0], video_dec_ctx->width, video_dec_ctx->height, QImage::Format_RGB32);

            if(isSignalConnected(QMetaMethod::fromSignal(&Demuxer::writeFrameImage))){
                FrameImage fi;
                fi.image=im;
                fi.frame=frame->coded_picture_number;
                emit writeFrameImage( fi);
            }

            if(isSignalConnected(QMetaMethod::fromSignal(&Demuxer::writeImage))){

                emit writeImage( im);
            }
            //            mutex.lock();
            //            m_runstate--;
            //            mutex.unlock();
            //            sem.acquire();

        }
    } else if (pkt.stream_index == audio_stream_idx) {
        /* decode audio frame */
        ret = avcodec_decode_audio4(audio_dec_ctx, frame, got_frame, &pkt);
        if (ret < 0) {
            fprintf(stderr, "Error decoding audio frame\n");
            return ret;
        }

        if (*got_frame) {
            printf("audio_frame%s n:%d nb_samples:%d pts:%s\n",
                   cached ? "(cached)" : "",
                   audio_frame_count++, frame->nb_samples,
                   av_ts2timestr(frame->pts, &audio_dec_ctx->time_base));

            ret = av_samples_alloc(audio_dst_data, &audio_dst_linesize, frame->channels,
                                   frame->nb_samples, (AVSampleFormat)frame->format, 1);
            if (ret < 0) {
                fprintf(stderr, "Could not allocate audio buffer\n");
                return AVERROR(ENOMEM);
            }

            /* TODO: extend return code of the av_samples_* functions so that this call is not needed */
            audio_dst_bufsize =
                    av_samples_get_buffer_size(NULL, frame->channels,
                                               frame->nb_samples,(AVSampleFormat) frame->format, 1);

            /* copy audio data to destination buffer:
             * this is required since rawaudio expects non aligned data */
            av_samples_copy(audio_dst_data, frame->data, 0, 0,
                            frame->nb_samples,
                            frame->channels,
                            (AVSampleFormat)frame->format
                            );

            /* write to rawaudio file */
            fwrite(audio_dst_data[0], 1, audio_dst_bufsize, audio_dst_file);
            av_freep(&audio_dst_data[0]);


        }
    }

    return ret;
}



int Demuxer::openCodecContext(int *stream_idx,
                              AVFormatContext *fmt_ctx, enum AVMediaType type)
{
    int ret;
    AVStream *st;
    AVCodecContext *dec_ctx = NULL;
    AVCodec *dec = NULL;

    ret = av_find_best_stream(fmt_ctx, type, -1, -1, NULL, 0);
    if (ret < 0) {
        fprintf(stderr, "Could not find %s stream in input file '%s'\n",
                av_get_media_type_string(type), src_filename);
        return ret;
    } else {
        *stream_idx = ret;
        st = fmt_ctx->streams[*stream_idx];

        /* find decoder for the stream */
        dec_ctx = st->codec;
        dec = avcodec_find_decoder(dec_ctx->codec_id);
        if (!dec) {
            fprintf(stderr, "Failed to find %s codec\n",
                    av_get_media_type_string(type));
            return ret;
        }

        if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
            fprintf(stderr, "Failed to open %s codec\n",
                    av_get_media_type_string(type));
            return ret;
        }
    }

    return 0;
}

int Demuxer::getFormatFromSampleFmt(const char **fmt,
                                    enum AVSampleFormat sample_fmt)
{
    int i;
    struct sample_fmt_entry {
        enum AVSampleFormat sample_fmt; const char *fmt_be, *fmt_le;
    } sample_fmt_entries[] = {
    { AV_SAMPLE_FMT_U8,  "u8",    "u8"    },
    { AV_SAMPLE_FMT_S16, "s16be", "s16le" },
    { AV_SAMPLE_FMT_S32, "s32be", "s32le" },
    { AV_SAMPLE_FMT_FLT, "f32be", "f32le" },
    { AV_SAMPLE_FMT_DBL, "f64be", "f64le" },
    { AV_SAMPLE_FMT_FLTP, "fltp", "fltp" },
};
    *fmt = NULL;

    for (i = 0; i < FF_ARRAY_ELEMS(sample_fmt_entries); i++) {
        struct sample_fmt_entry *entry = &sample_fmt_entries[i];
        if (sample_fmt == entry->sample_fmt) {
            *fmt = AV_NE(entry->fmt_be, entry->fmt_le);
            return 0;
        }
    }

    fprintf(stderr,
            "sample format %s is not supported as output format\n",
            av_get_sample_fmt_name(sample_fmt));
    return -1;
}

int Demuxer::init (char **argv)
{
    src_filename = argv[0];
    audio_dst_filename = argv[1];
    return  0;
}

void Demuxer::process(){
    qDebug() << "started demuxer: " << QObject::thread();
    int ret = 0, got_frame;
    /* register all formats and codecs */
    av_register_all();


    /* open input file, and allocate format context */
    if (avformat_open_input(&fmt_ctx, src_filename, NULL, NULL) < 0) {
        fprintf(stderr, "Could not open source file %s\n", src_filename);
        exit(1);
    }

    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
        fprintf(stderr, "Could not find stream information\n");
        exit(1);
    }

    if (openCodecContext(&video_stream_idx, fmt_ctx, AVMEDIA_TYPE_VIDEO) >= 0) {
        video_stream = fmt_ctx->streams[video_stream_idx];
        video_dec_ctx = video_stream->codec;



        /* allocate image where the decoded image will be put */
        ret = av_image_alloc(video_dst_data, video_dst_linesize,
                             video_dec_ctx->width, video_dec_ctx->height,
                             video_dec_ctx->pix_fmt, 1);
        if (ret < 0) {
            fprintf(stderr, "Could not allocate raw video buffer\n");
            goto end;
        }
        video_dst_bufsize = ret;
    }

    if (openCodecContext(&audio_stream_idx, fmt_ctx, AVMEDIA_TYPE_AUDIO) >= 0) {
        int nb_planes;

        audio_stream = fmt_ctx->streams[audio_stream_idx];
        audio_dec_ctx = audio_stream->codec;
        audio_dst_file = fopen(audio_dst_filename, "wb");
        if (!audio_dst_file) {
            fprintf(stderr, "Could not open destination file %s\n", audio_dst_filename);
            ret = 1;
            goto end;
        }

        nb_planes = av_sample_fmt_is_planar(audio_dec_ctx->sample_fmt) ?
                    audio_dec_ctx->channels : 1;
        audio_dst_data =(uint8_t **) av_mallocz(sizeof(uint8_t *) * nb_planes);
        if (!audio_dst_data) {
            fprintf(stderr, "Could not allocate audio data buffers\n");
            ret = AVERROR(ENOMEM);
            goto end;
        }
    }

    /* dump input information to stderr */
    av_dump_format(fmt_ctx, 0, src_filename, 0);

    if (!audio_stream && !video_stream) {
        fprintf(stderr, "Could not find audio or video stream in the input, aborting\n");
        ret = 1;
        goto end;
    }

    frame = avcodec_alloc_frame();
    if (!frame) {
        fprintf(stderr, "Could not allocate frame\n");
        ret = AVERROR(ENOMEM);
        goto end;
    }

    /* initialize packet, set data to NULL, let the demuxer fill it */
    av_init_packet(&pkt);
    pkt.data = NULL;
    pkt.size = 0;

    //    if (video_stream)
    //        printf("Demuxing video from file '%s' into '%s'\n", src_filename, video_dst_filename);
    if (audio_stream)
        printf("Demuxing audio from file '%s' into '%s'\n", src_filename, audio_dst_filename);

    /* read frames from the file */
    while (av_read_frame(fmt_ctx, &pkt) >= 0) {

        decodePacket(&got_frame, 0);
        av_free_packet(&pkt);
    }

    /* flush cached frames */
    pkt.data = NULL;
    pkt.size = 0;
    do {
        decodePacket(&got_frame, 1);
    } while (got_frame);

    printf("Demuxing succeeded.\n");

    //    if (video_stream) {
    //        printf("Play the output video file with the command:\n"
    //               "ffplay -f rawvideo -pix_fmt %s -video_size %dx%d %s\n",
    //               av_get_pix_fmt_name(video_dec_ctx->pix_fmt), video_dec_ctx->width, video_dec_ctx->height,
    //               video_dst_filename);
    //    }

    if (audio_stream) {
        const char *fmt;

        if ((ret = getFormatFromSampleFmt(&fmt, audio_dec_ctx->sample_fmt)) < 0)
            goto end;
        printf("Play the output audio file with the command:\n"
               "ffplay -f %s -ac %d -ar %d %s\n",
               fmt, audio_dec_ctx->channels, audio_dec_ctx->sample_rate,
               audio_dst_filename);
    }

end:
    if (video_dec_ctx)
        avcodec_close(video_dec_ctx);
    if (audio_dec_ctx)
        avcodec_close(audio_dec_ctx);
    avformat_close_input(&fmt_ctx);
    if (audio_dst_file)
        fclose(audio_dst_file);
    av_free(frame);
    av_free(video_dst_data[0]);
    av_free(audio_dst_data);


}


