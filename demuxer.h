#ifndef DEMUXER_H
#define DEMUXER_H

#include <QObject>
#include <QDebug>
#include <QMutex>
#include <QThread>
#include <ffmpegcommon.h>
#include <QSemaphore>
#include <QMetaMethod>
#include <QImage>
#include <frameimage.h>

class Demuxer : public QObject
{
    Q_OBJECT
public:
    explicit Demuxer(QObject *parent);



signals:
    //    void writeRgb24(AVVideoFrame);
    void writeImage(QImage);
    void writeFrameImage(FrameImage);
public slots:
    int init(char **argv);
    void process();
    void setRequestFrames(int arg)
    {
        //        mutex.lock();
        sem.release(arg);
        //        m_runstate = arg;
        //         mutex.unlock();
    }

private:

    int openCodecContext(int *stream_idx, AVFormatContext *fmt_ctx, AVMediaType type);
    int decodePacket(int *got_frame, int cached);
    int getFormatFromSampleFmt(const char **fmt, AVSampleFormat sample_fmt);

    AVFormatContext *fmt_ctx;
    AVCodecContext *video_dec_ctx, *audio_dec_ctx;
    AVStream *video_stream, *audio_stream;
    const char *src_filename;

    const char *audio_dst_filename;

    FILE *audio_dst_file;

    uint8_t *video_dst_data[4];
    int      video_dst_linesize[4];
    int video_dst_bufsize;

    uint8_t **audio_dst_data;
    int       audio_dst_linesize;
    int audio_dst_bufsize;

    int video_stream_idx, audio_stream_idx;
    AVFrame *frame;
    AVPacket pkt;
    int video_frame_count;
    int audio_frame_count;

    QSemaphore sem;
    QMutex mutex;

    int out_size;
    uint8_t  *dst_data[4];
    int dst_linesize[4];
    struct SwsContext *sws_ctx;
    double pts;
    // int m_runstate;
};

#endif // DEMUXER_H
