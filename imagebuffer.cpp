#include "imagebuffer.h"

ImageBuffer::ImageBuffer(QObject *parent) :
    QObject(parent),
    ctr(0)
{
    sem.release(10);
}

void ImageBuffer::writeSlot(QImage data)
{
    sem.acquire();
    images.enqueue(new QImage(data.copy()));

}


void ImageBuffer::frameClock()
{


    if(images.isEmpty())return;
    qDebug() << "write thread: " << this->thread() << ctr++;

    QImage *im=images.dequeue();
    emit videoOutput(*im);
    delete im;
    sem.release();

}
