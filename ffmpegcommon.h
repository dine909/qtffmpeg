#ifndef FFMPEGCOMMON_H
#define FFMPEGCOMMON_H

#include <math.h>

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#include <libswscale/swscale.h>

#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>

#ifdef __cplusplus
}
#endif //__cplusplus
#include <decoder.h>
#include <demuxer.h>

#define THREAD_TICK 33
#endif // FFMPEGCOMMON_H
