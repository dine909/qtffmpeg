#include "avgraphicswidget.h"

AVGraphicsWidget::AVGraphicsWidget(QWidget *parent) :
    QGraphicsView(parent)
{

    this->setScene(&scene);

}

void AVGraphicsWidget::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);
    setSceneRect(QRectF(QPointF(0.0,0.0), event->size()));
}

