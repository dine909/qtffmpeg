#-------------------------------------------------
#
# Project created by QtCreator 2013-01-22T22:32:27
#
#-------------------------------------------------

isEmpty(PROJECTROOT): PROJECTROOT = $$PWD

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtFFMPEGExample
TEMPLATE = app

INCLUDEPATH += $${PROJECTROOT}/../root/include /opt/local/include
LIBS +=  -L$${PROJECTROOT}/../root/lib -L/opt/local/lib

LIBS += -Lextra -lavcodec -lavformat -lavutil -lswscale
LIBS += -ljack -ljackserver


SOURCES += main.cpp\
        mainwindow.cpp \
    demuxer.cpp \
    avgraphicswidget.cpp \
    avvideoobject.cpp \
    frametimer.cpp \
    jackshowtime.cpp \
    jackproxythread.cpp \
    frameimagebuffer.cpp \
    imagebuffer.cpp

HEADERS  += mainwindow.h \
    demuxer.h \
    ffmpegcommon.h \
    avgraphicswidget.h \
    avvideoobject.h \
    frametimer.h \
    jackshowtime.h \
    jackcommon.h \
    jackproxythread.h \
    frameimage.h \
    frameimagebuffer.h \
    imagebuffer.h
