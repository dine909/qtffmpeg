#ifndef DECODER_H
#define DECODER_H

#include <QObject>
#include <ffmpegcommon.h>

#define INBUF_SIZE 4096
#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096

class Decoder : public QObject
{
    Q_OBJECT
public:
    explicit Decoder(QObject *parent = 0);
    void videoDecodeExample(const char *outfilename, const char *filename);
    int decodeWriteFrame(const char *outfilename, AVCodecContext *avctx, AVFrame *frame, int *frame_count, AVPacket *pkt, int last);
    void pgmSave(unsigned char *buf, int wrap, int xsize, int ysize, char *filename);

signals:
    
public slots:

};

#endif // DECODER_H
