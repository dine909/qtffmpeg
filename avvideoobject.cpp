#include "avvideoobject.h"
#include <QtGui/QPainter>
#include <QGraphicsScene>

AVVideoObject::AVVideoObject(QGraphicsItem *parent) : QGraphicsObject(parent)
{

}

void AVVideoObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if (!image.isNull()) {
        if (image.size() == QSize(iwidth, iheight))
            painter->drawImage(QPointF(0,0), image);
        else
            painter->drawImage(boundingRect(), image);
    }

}

//void AVVideoObject::videoInput(AVVideoFrame frame)
//{

//    iwidth=frame.width;
//    iheight=frame.height;
//    image= QImage((uchar*)frame.data.data(), frame.width, frame.height, QImage::Format_RGB32);
//    scene()->update();
//}

void AVVideoObject::videoInput(QImage frame)
{
    image=QImage(frame.copy());
    iwidth=frame.width();
    iheight=frame.height();
    scene()->update();
}

QRectF AVVideoObject::boundingRect() const
{
    return QRectF(0, 0, iwidth, iheight);
}
