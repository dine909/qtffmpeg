#ifndef IMAGEBUFFER_H
#define IMAGEBUFFER_H

#include <QObject>
#include <QDebug>
#include <QImage>
#include <QSemaphore>
#include <QQueue>

class ImageBuffer : public QObject
{
    Q_OBJECT
    QSemaphore sem;
     QSemaphore dsem;
    QQueue<QImage *>images;
     int ctr;
public:
    explicit ImageBuffer(QObject *parent = 0);
    
signals:
    void videoOutput(QImage);
public slots:
    void frameClock();
    virtual void writeSlot(QImage data);
};

#endif // IMAGEFRAMEBUFFER_H
