#ifndef JACKSHOWTIME_H
#define JACKSHOWTIME_H

#include <QObject>
#include <QThread>
#include "jackcommon.h"
#include "jackproxythread.h"
#include <QSemaphore>

class JackShowTime : public QObject
{
    Q_OBJECT
    jack_client_t *client;

    static void callback(jack_transport_state_t state,
                        jack_nframes_t nframes,
                        jack_position_t *pos,
                        int new_pos,
                        void *arg);
    QSemaphore sem;

    jack_position_t current;
    jack_transport_state_t transport_state;
    jack_nframes_t frame_time;
    int wi;
    ulong vframe,lastvframe;
    double time;
public:
    JackProxyThread pro;
    explicit JackShowTime(QObject *parent = 0);
public slots:
    void callbackSlot(jack_transport_state_t,
                      jack_nframes_t,
                      jack_position_t *,
                      int ,
                      void *);
    void process();
signals:
    void frameClock();
    void frameClock(ulong);
};

#endif // JACKSHOWTIME_H
