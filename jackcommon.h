#ifndef JACKCOMMON_H
#define JACKCOMMON_H

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include <jack/jack.h>
#include <jack/types.h>
#include <jack/transport.h>


#ifdef __cplusplus
}
#endif //__cplusplus

#endif // JACKCOMMON_H
