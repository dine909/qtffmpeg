#include "jackshowtime.h"

JackShowTime::JackShowTime(QObject *parent) :  QObject(parent),
    wi(0), vframe(0),lastvframe(-1)
{

}
void JackShowTime::callbackSlot(jack_transport_state_t ts, jack_nframes_t ft, jack_position_t * jp, int, void *){
    current=*jp;
    transport_state=ts;
    frame_time=ft;

    sem.release();
}

void gen_random(char *s, const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}
void JackShowTime::process()
{
    char nm[9];
    gen_random(nm,8);
    connect(JackProxyThread::singleton(),SIGNAL(callbackSignal(jack_transport_state_t,jack_nframes_t,jack_position_t*,int,void*)),
            this,SLOT(callbackSlot(jack_transport_state_t,jack_nframes_t,jack_position_t*,int,void*)),Qt::DirectConnection);

    if ((client = jack_client_open (nm, JackNullOption, NULL)) == 0) {
        fprintf (stderr, "JACK server not running?\n");
        return;
    }

    jack_set_timebase_callback(client,0,callback,0);
    jack_activate(client);

    forever{
        sem.acquire();
        // transport_state = jack_transport_query (client, &current);
        //frame_time = jack_frame_time (client);
        time=(current.frame/frame_time)/((double)current.frame_rate/(double)frame_time);

        vframe=time*30.0;
        if(vframe!=lastvframe){
            lastvframe=vframe;
            emit frameClock();
            emit frameClock(vframe);
        }else
        {
            continue;
        }
        printf ("vframe: %d time %.2f frame = %u  frame_time = %u usecs = %lld \t",
                vframe,
                (current.frame/frame_time)/(44100.0/512.0),
                current.frame,
                frame_time,
                current.usecs);

        switch (transport_state) {
        case JackTransportStopped:
            printf ("state: Stopped");
            break;
        case JackTransportRolling:
            printf ("state: Rolling");
            break;
        case JackTransportStarting:
            printf ("state: Starting");
            break;
        default:
            printf ("state: [unknown]");
        }

        if (current.valid & JackPositionBBT)
            printf ("\tBBT: %3" PRIi32 "|%" PRIi32 "|%04"
                    PRIi32, current.bar, current.beat, current.tick);

        if (current.valid & JackPositionTimecode)
            printf ("\tTC: (%.6f, %.6f)",
                    current.frame_time, current.next_time);


        printf ("\n");

        //    QThread::usleep(100);

    }
}

void JackShowTime::callback(jack_transport_state_t state,
                            jack_nframes_t nframes,
                            jack_position_t *pos,
                            int new_pos,
                            void *arg){
    JackProxyThread::singleton()->callback( state,
                                            nframes,
                                            pos,
                                            new_pos,
                                            arg);
}
