#include "frameImageBuffer.h"

FrameImageBuffer::FrameImageBuffer(QObject *parent) :
    QObject(parent),
    ctr(0)
{
    sem.release(10);
}

void FrameImageBuffer::writeSlot(FrameImage data)
{
    sem.acquire();
    QImage fr=data.image.copy();
    FrameImage fi;
    fi.image=fr;
    fi.frame=data.frame;
    images.enqueue(fi);

}


void FrameImageBuffer::frameClock()
{


    if(images.isEmpty())return;
    qDebug() << "write thread: " << this->thread() << ctr++;

    FrameImage fi=images.dequeue();
    emit videoOutput(fi.image);
    delete &fi.image;
    sem.release();

}

void FrameImageBuffer::frameClock(ulong frame)
{
    if(images.isEmpty())return;
    FrameImage fi=images.dequeue();

 //   Q_ASSERT(fi.frame==(frame));
//    if(fi.frame==(frame)){
        emit videoOutput(fi.image);
//    }

//    delete fi.image;
    sem.release();

}

